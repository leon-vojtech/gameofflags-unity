﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class FloorChangeController : MonoBehaviour
{
    public GameObject[] floors;
    public GameObject[] terrains;
    public Button[] floorButtons;

    private float timer;
    private bool changedManually;

    private void Start()
    {
        InvokeRepeating("SetFloorByPosition", 0.1f, 1f);
        floorButtons[GetFloor(PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR)) - 1].interactable = false;
    }

    private void Update()
    {
        // if player clicked to follow camera, then immediately show player's floor
        if (PlayerPrefs.GetInt(C.FOLLOW_CAMERA, 0) == 1)
        {
            changedManually = false;
            timer = 0;
            SetFloorByPosition();
        }

        if (changedManually)
        {
            timer += Time.deltaTime;
            if (timer > 60)
            {
                changedManually = false;
                timer = 0;
            }
        }
    }
    // called by touching buttons with floor
    public void SetFloor(int floor)
    {
        changedManually = true;
        timer = 0;
        // change floor even if player is followed
        PlayerPrefs.SetInt(C.FOLLOW_CAMERA, 0);
        ChangeFloor(floor);
    }

    private void ChangeFloor(int floor)
    {    
        // change floor only if selected floor is not requested floor
        if (GetFloor(PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR)) != floor)
        {
            // make selected button disable
            if (floor <= floorButtons.Length)
            {
                floorButtons[GetFloor(PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR)) - 1].interactable = true;
                floorButtons[floor - 1].interactable = false;
            }
            if (floor <= terrains.Length)
            {
                terrains[GetFloor(PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR)) - 1].SetActive(false);
                terrains[floor - 1].SetActive(true);
            }
            if (floor <= floors.Length) 
            {
                floors[floor - 1].SetActive(true);
                floors[GetFloor(PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR)) - 1].SetActive(false);
                // set selected floor - used by towers, onlinePlayers and player controller
                PlayerPrefs.SetString(C.SELECTED_FLOOR, GetFloorName(floor));
                TowersController.Instance.SetTowersData();
                OnlinePlayersController.Instance.SetOnlinePlayersData();
            }
        }
    }

    private string GetFloorName(int floor)
    {
        return "J" + floor + "NP";
    }
    
    // retrieves floor from string
    private int GetFloor(string floorName)
    {
        return int.Parse(Regex.Match(floorName, @"\d+").Value);
    }

    // set real position only if player do not select other floor in last minute
    private void SetFloorByPosition()
    {
        if (!changedManually)
        {
            ChangeFloor(GetFloor(PlayerPrefs.GetString(C.PLAYER_FLOOR, C.DEFAULT_FLOOR)));
        }
    }
}
