﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TowersController : MonoBehaviour
{
    public GameObject towerPrefab;
    public Material materialRed;
    public Material materialBlue;
    public GameObject towerInfoPanel;

    public TextMeshProUGUI tmpName;
    public TextMeshProUGUI tmpHP;
    public TextMeshProUGUI tmpLevel;
    public TextMeshProUGUI tmpFraction;
    public TextMeshProUGUI tmpCapturedBy;
    public TextMeshProUGUI tmpCapturedAt;
    public Button btnAttack;
    public Button btnRepair;
    public Button btnSignIn;
    public TextMeshProUGUI tmpActionPossibleMessage;
    public TextMeshProUGUI tmpRedScore;
    public TextMeshProUGUI tmpBlueScore;

    private AndroidJavaObject databaseService;

    private Dictionary<string, Tower> towersDictionary = new Dictionary<string, Tower>();

    private bool wasTowerClicked; // if any tower was touched this time on this scene, its for FillTowerPanel
    private Tower towerUnderFinger; // used for check if tower on touch began is the same as on touch end ( not moved finger out)

    private static TowersController _instance;
    public static TowersController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<TowersController>();
            }

            return _instance;
        }
    }

    private void Start()
    {
        databaseService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getDatabaseInstance");

        InvokeRepeating("SetTowersData", 0.1f, 1f);
        InvokeRepeating("SetFractionsScore", 0.1f, 1f);
        InvokeRepeating("FillTowerPanel", 0.1f, 1f);
    }

    private void Update()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            //if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))  -- does not work! with this panels are clicked through
            if (!IsPointerOverUIObject() && touch.phase == TouchPhase.Began)
            {
                RaycastHit hitInfo = new RaycastHit();
                bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 1000, 1 << 11);
                if (hit)
                {
                    towersDictionary.TryGetValue(hitInfo.transform.parent.gameObject.name, out towerUnderFinger);
                }
            }
            else if (!IsPointerOverUIObject() && touch.phase == TouchPhase.Moved)
            {
                if (Mathf.Abs(Mathf.Pow(touch.deltaPosition.x, 2) + Mathf.Pow(touch.deltaPosition.y, 2)) > 10)
                {
                    // if finger moves too much, then just move camera and do nothing here
                    towerUnderFinger = null;
                }
            }
            else if ( towerUnderFinger != null && !IsPointerOverUIObject() && touch.phase == TouchPhase.Ended)
            {
                RaycastHit hitInfo = new RaycastHit();
                bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo, 1000, 1 << 11);
                if (hit)
                {
                    towersDictionary.TryGetValue(hitInfo.transform.parent.gameObject.name, out Tower selectedTower);
                    if (towerUnderFinger == selectedTower)
                    {
                        // Save selected tower for attack / repair and tower info
                        SharedObjects.AR_TOWER = selectedTower;
                        wasTowerClicked = true;
                        FillTowerPanel();
                        towerInfoPanel.SetActive(true);
                    }
                }
            }
        }
    }

    // http://answers.unity.com/answers/1115473/view.html
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public void SetTowersData()
    {
        Dictionary<string, Tower> newTowersDictionary = GetTowersDictionary();
        // remove deleted towers 
        RemovePairDictionary(towersDictionary, newTowersDictionary);
        // get towers info from db
        AddOrUpdatePairDictionary(towersDictionary, newTowersDictionary);
        
        // create towers

        foreach (KeyValuePair<string, Tower> entry in towersDictionary)
        {
            Tower tower = entry.Value;
            if (tower.Instance == null)
            {
                // Instantiate at obtained position and zero rotation.
                tower.Instance = Instantiate(towerPrefab, new Vector3(tower.X / 100, 0, -tower.Y / 100), Quaternion.identity);
                tower.Instance.name = entry.Key;
            } else
            {
                tower.Instance.transform.position = new Vector3(tower.X / 100, 0, -tower.Y / 100);
            }

            if (tower.Fraction == 1)
            {
                ChangeMaterial(tower.Instance, materialRed);
            } else if (tower.Fraction == 2)
            {
                ChangeMaterial(tower.Instance, materialBlue);
            }
        }
    }

    private Dictionary<string, Tower> GetTowersDictionary()
    {
        Dictionary<string, Tower> newTowersDictionary = new Dictionary<string, Tower>();
        AndroidJavaObject towers = databaseService.Call<AndroidJavaObject>("getTowersByFloor", PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR));
        if (towers.Call<int>("size") > 0)
        {
            AndroidJavaObject iterator = towers.Call<AndroidJavaObject>("entrySet").Call<AndroidJavaObject>("iterator");
            while (iterator.Call<bool>("hasNext"))
            {
                AndroidJavaObject pair = iterator.Call<AndroidJavaObject>("next");
                newTowersDictionary.Add(pair.Call<string>("getKey"), new Tower(pair.Call<AndroidJavaObject>("getValue"), pair.Call<string>("getKey")));
                iterator.Call("remove");
            }
        }

        return newTowersDictionary;
    }

    private void ChangeMaterial(GameObject tower, Material newMat)
    {
        // children 11 is upper part of tower
        Renderer[] children = tower.GetComponentsInChildren<Renderer>();
        var mats = new Material[children[11].materials.Length];
        for (var j = 0; j < children[11].materials.Length; j++)
        {
            mats[j] = newMat;
        }
        children[11].materials = mats;
    }

    private void AddOrUpdatePairDictionary(Dictionary<string, Tower> dic, Dictionary<string, Tower> newDic)
    {
        foreach (KeyValuePair<string, Tower> entry in newDic)
        {
            Tower tower;
            if (dic.TryGetValue(entry.Key, out tower))
            {
                // value exists
                entry.Value.Instance = tower.Instance;
                dic[entry.Key] = entry.Value;
            }
            else
            {
                // add the value
                dic.Add(entry.Key, entry.Value);
            }
        }
    }

    private void RemovePairDictionary(Dictionary<string, Tower> dic, Dictionary<string, Tower> newDic)
    {
        Dictionary<string, Tower> dicCopy = new Dictionary<string, Tower>(dic);
        foreach (KeyValuePair<string, Tower> entry in dicCopy)
        {
            if (!newDic.ContainsKey(entry.Key))
            {
                // remove tower object
                Destroy(entry.Value.Instance);
                dic.Remove(entry.Key);
            }
        }
    }

    public void Attack()
    {
        SharedObjects.AR_ACTION = C.AR_ATTACK;
        SharedObjects.AR_TOWER.Instance = null;
        Destroy(gameObject); // it must be destroyed manually, because of NPE from SettingsController (objects do not exist)
        SceneManager.LoadScene(C.AR_SCENE);
    }

    public void Repair()
    {
        SharedObjects.AR_ACTION = C.AR_REPAIR;
        SharedObjects.AR_TOWER.Instance = null;
        Destroy(gameObject); // it must be destroyed manually, because of NPE from SettingsController (objects do not exist)
        SceneManager.LoadScene(C.AR_SCENE);
    }

    private void FillTowerPanel()
    {
        if (wasTowerClicked)
        {
            // use SharedObjects.AR_TOWER instead of selectedTower - because of npe after login from this panel
            Tower tower = SharedObjects.AR_TOWER;

            tmpName.text = tower.Name;
            tmpHP.text = tower.HealthPoints + "/" + DataCounter.CalculateTowerMaxHealthPoints(tower.Level);
            tmpLevel.text = tower.Level.ToString();
            if (tower.Fraction == 1)
            {
                tmpFraction.text = C.FRACTION_RED;
            }
            else
            {
                tmpFraction.text = C.FRACTION_BLUE;
            }
            tmpCapturedBy.text = tower.CapturedBy;
            if (tower.CapturedAt == 0)
            {
                tmpCapturedAt.text = "Nikdy";
            }
            else
            {
                tmpCapturedAt.text = DateTimeOffset.FromUnixTimeMilliseconds(tower.CapturedAt).ToLocalTime().ToString("d. M. yyyy HH:mm:ss");
            }

            AndroidJavaObject player = databaseService.Call<AndroidJavaObject>("getPlayer");
            // check if player exists, or show signIn
            if (databaseService.Call<string>("getUserUid") != null && player != null)
            {
                // check if player can attack or repair and if tower is attackable now
                double timeNow = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
                if (player.Call<int>("getFraction") != tower.Fraction) // attack
                {
                    if (tower.CapturedAt + C.MINUTES_10 > timeNow)
                    {
                        tmpActionPossibleMessage.text = "Možný útok v " + DateTimeOffset.FromUnixTimeMilliseconds(tower.CapturedAt + C.MINUTES_10).ToLocalTime().ToString("HH:mm:ss");
                        SetAttackRepairActionsVisibility(false, false, false, true);
                    }
                    else if (player.Call<long>("getLastAttackTimestamp") + C.MINUTES_10 > timeNow)
                    {
                        tmpActionPossibleMessage.text = "Další možný útok v " + DateTimeOffset.FromUnixTimeMilliseconds(player.Call<long>("getLastAttackTimestamp") + C.MINUTES_10).ToLocalTime().ToString("HH:mm:ss");
                        SetAttackRepairActionsVisibility(false, false, false, true);
                    }
                    else
                    {
                        SetAttackRepairActionsVisibility(false, true, false, false);
                    }

                }
                else // repair
                {
                    if (tower.HealthPoints >= DataCounter.CalculateTowerMaxHealthPoints(tower.Level))
                    {
                        tmpActionPossibleMessage.text = "Věž je zcela opravena";
                        SetAttackRepairActionsVisibility(false, false, false, true);
                    }
                    else if (player.Call<long>("getLastRepairTimestamp") + C.MINUTES_10 > timeNow)
                    {
                        tmpActionPossibleMessage.text = "Další možná oprava v " + DateTimeOffset.FromUnixTimeMilliseconds(player.Call<long>("getLastRepairTimestamp") + C.MINUTES_10).ToLocalTime().ToString("HH:mm:ss");
                        SetAttackRepairActionsVisibility(false, false, false, true);
                    }
                    else
                    {
                        SetAttackRepairActionsVisibility(false, false, true, false);
                    }
                }
            }
            else
            {
                SetAttackRepairActionsVisibility(true, false, false, false);
            }
        }
    }

    private void SetAttackRepairActionsVisibility(bool signIn, bool attack, bool repair, bool message)
    {
        btnSignIn.gameObject.SetActive(signIn);
        btnAttack.gameObject.SetActive(attack);
        btnRepair.gameObject.SetActive(repair);
        tmpActionPossibleMessage.gameObject.SetActive(message);
    }

    private void SetFractionsScore()
    {
        tmpRedScore.text = databaseService.Call<int>("getTowersCountForFraction", 1).ToString();
        tmpBlueScore.text = databaseService.Call<int>("getTowersCountForFraction", 2).ToString();
    }
}
