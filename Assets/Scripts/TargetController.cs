﻿using UnityEngine;
using Vuforia;

public class TargetController : MonoBehaviour, ITrackableEventHandler
{
    protected TrackableBehaviour trackableBehaviour;

    private GameObject tower;

    public GameObject towerPrefab;
    public Material materialRed;
    public Material materialBlue;

    protected virtual void Start()
    {
        trackableBehaviour = GetComponent<TrackableBehaviour>();
        if (trackableBehaviour)
            trackableBehaviour.RegisterTrackableEventHandler(this);
    }

    protected virtual void OnDestroy()
    {
        if (trackableBehaviour)
            trackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + trackableBehaviour.TrackableName + " found");
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            Debug.Log("Trackable " + trackableBehaviour.TrackableName + " lost");
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }


    protected virtual void OnTrackingFound()
    {
        if (trackableBehaviour.TrackableName.Equals(SharedObjects.AR_TOWER.ArTarget))
        {
            SharedObjects.IS_DETECTED = true;
            tower = Instantiate(towerPrefab, new Vector3(), Quaternion.identity, transform);
            tower.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
            tower.transform.Rotate(new Vector3(90, 0, 0));
            tower.transform.position = new Vector3(0, 0, -0.5f);
            SetTowerMaterial();
        }
    }


    protected virtual void OnTrackingLost()
    {
        if (trackableBehaviour.TrackableName.Equals(SharedObjects.AR_TOWER.ArTarget))
        {
            SharedObjects.IS_DETECTED = false;
            Destroy(tower);
        }
    }

    private void SetTowerMaterial()
    {
        if (tower != null)
        {
            if (SharedObjects.AR_TOWER.Fraction == 1)
            {
                ChangeMaterial(tower, materialRed);
            }
            else if (SharedObjects.AR_TOWER.Fraction == 2)
            {
                ChangeMaterial(tower, materialBlue);
            }
        }
    }

    private void ChangeMaterial(GameObject tower, Material newMat)
    {
        // children 11 is upper part of tower
        Renderer[] children = tower.GetComponentsInChildren<Renderer>();
        var mats = new Material[children[11].materials.Length];
        for (var j = 0; j < children[11].materials.Length; j++)
        {
            mats[j] = newMat;
        }
        children[11].materials = mats;
    }


}
