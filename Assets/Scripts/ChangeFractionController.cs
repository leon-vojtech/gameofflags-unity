﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangeFractionController : MonoBehaviour
{
    private AndroidJavaObject databaseService;
    
    public TextMeshProUGUI tmpFraction;
    public TextMeshProUGUI tmpLastChange;
    public TextMeshProUGUI tmpChangePossible;
    public Button btnChangeFraction;
    
    private void Start()
    {
        databaseService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getDatabaseInstance");
    }
    
    private void Update()
    {
        if (!databaseService.Call<bool>("isPlayerFractionChanged"))
        {
            LoadingPanelController.Instance.StartLoading("changeFraction");
        }
        else
        {
            StartCoroutine(StopLoadingAndFillPanel());
        }
    }

    public void ChangeFraction()
    {
        databaseService.Call("changeFraction");
    }

    public void FillChangeFractionPanel()
    {
        AndroidJavaObject player = getPlayer();
        if (player != null)
        {
            if (player.Call<int>("getFraction") == 2)
            {
                tmpFraction.text = "Modří";
            }
            else if (player.Call<int>("getFraction") == 1)
            {
                tmpFraction.text = "Červení";
            }
            else
            {
                tmpFraction.text = "chyba";
            }

            if (player.Call<long>("getLastChangeFractionAt") + C.WEEK_MILLISECONDS < new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds())
            {
                btnChangeFraction.interactable = true;
                tmpChangePossible.text = "Nyní";
            }
            else
            {
                btnChangeFraction.interactable = false;
                tmpChangePossible.text = DateTimeOffset.FromUnixTimeMilliseconds(player.Call<long>("getLastChangeFractionAt") + C.WEEK_MILLISECONDS).ToLocalTime().ToString("d. M. yyyy HH:mm:ss");
            }
            if (player.Call<long>("getLastChangeFractionAt") == 0)
            {
                tmpLastChange.text = "Nikdy";
            }
            else
            {
                tmpLastChange.text = DateTimeOffset.FromUnixTimeMilliseconds(player.Call<long>("getLastChangeFractionAt")).ToLocalTime().ToString("d. M. yyyy HH:mm:ss");
            }
        }
    }

    private AndroidJavaObject getPlayer()
    {
        return databaseService.Call<AndroidJavaObject>("getPlayer");
    }

    private IEnumerator StopLoadingAndFillPanel()
    {
        //Wait for 1 second
        yield return new WaitForSecondsRealtime(1);

        LoadingPanelController.Instance.StopLoading("changeFraction");
        FillChangeFractionPanel();
    }
}
