﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class LoadingPanelController : MonoBehaviour
{
    public GameObject loadingPanel;
    public GameObject loadingRetryPanel;
    public Image loadingImage;
    private float rotateSpeed = 5f;

    private float loadingTime;
    private bool loading;

    private List<string> actionNameLoading;

    private static LoadingPanelController _instance;
    public static LoadingPanelController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LoadingPanelController>();
            }

            return _instance;
        }
    }

    private void Start()
    {
        actionNameLoading = new List<string>();
    }

    private void Update()
    {
        if (loading)
        {
            PrepareLoading();
        } 
        else
        {
            loadingPanel.SetActive(false);
        }
    }

    private void PrepareLoading()
    {
        loadingPanel.SetActive(true);
        loadingTime += Time.deltaTime;

        // create animation of loading
        loadingImage.transform.Rotate(0f, 0f, -(rotateSpeed + Time.deltaTime));

        if (loadingTime > C.MAX_LOADING_TIME)
        {
            loadingRetryPanel.SetActive(true);
        } else
        {
            loadingRetryPanel.SetActive(false);
        }
    }

    public void StartLoading(string action)
    {
        loading = true;
        if (!actionNameLoading.Exists(a => a.Equals(action))) {
            actionNameLoading.Add(action);
        }
    }

    public void StopLoading(string action)
    {
        if (actionNameLoading.Exists(a => a.Equals(action)))
        {
            actionNameLoading.Remove(action);
        }
        if (!actionNameLoading.Any())
        {
            loading = false;
            ResetLoadingTime();
        }
    }

    public void ResetLoadingTime()
    {
        loadingTime = 0;
    }

}
