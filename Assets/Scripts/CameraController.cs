﻿using UnityEngine;
using UnityEngine.EventSystems;

/**
 * Controller for changing position of camera by gestures and touches
 * part of inspiration at:
 * zoom - https://unity3d.com/learn/tutorials/topics/mobile-touch/pinch-zoom
 * pan - https://stackoverflow.com/a/27836741
 * rotate - http://answers.unity.com/answers/440475/view.html
 */
public class CameraController : MonoBehaviour
{
    private new Camera camera;

    private void Start()
    {
        camera = Camera.main; // even though script is attached to camera, we need instance for fov
    }

    private Vector3 worldStartPoint;
    private readonly float perspectiveZoomSpeed = 0.2f;
    private readonly float tiltSpeed = 0.2f;
    private bool isRotate, isTilt, isZoom, wasMoreThenOneTouch;
    private float oldAngle;
    private float rotateAroundPointState = -1; // default value
    private Vector3 pointPosition;

    void LateUpdate()
    {
        // if the touch is on UI element
        if (Input.touchCount > 0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        {
            // still needs to know where the touch began, because of moving out of UI elements
            worldStartPoint = GetWorldPoint(Input.GetTouch(0).position);
            return;
        }
        // pan
        if (Input.touchCount == 1)
        {
            Touch currentTouch = Input.GetTouch(0);
            if (currentTouch.phase == TouchPhase.Began || wasMoreThenOneTouch)
            {
                worldStartPoint = GetWorldPoint(currentTouch.position);
                wasMoreThenOneTouch = false;
                isRotate = false;
                isTilt = false;
                isZoom = false;
            }
            else if (currentTouch.phase == TouchPhase.Moved)
            {
                Vector3 worldDelta = worldStartPoint - GetWorldPoint(currentTouch.position);
                // limit camera position
                if (Mathf.Abs(transform.position.x + worldDelta.x) <= C.MAX_CAMERA_POSITION && Mathf.Abs(transform.position.z + worldDelta.z) <= C.MAX_CAMERA_POSITION)
                {
                    transform.Translate(worldDelta.x, 0, worldDelta.z, Space.World);
                }
                else
                {
                    worldStartPoint = GetWorldPoint(currentTouch.position);
                }
            }
        }
        // end pan
        else if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Began of two touches
            if (!wasMoreThenOneTouch)
            {
                oldAngle = CalculateAngleBetweenTouches(touchZero, touchOne);
                wasMoreThenOneTouch = true;
            }

            if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved)
            {
                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                float newAngle = CalculateAngleBetweenTouches(touchZero, touchOne);
                float deltaAngle = Mathf.DeltaAngle(oldAngle, newAngle);
                oldAngle = newAngle;

                // if there is not any action for two touches
                if (!isTilt && !isZoom && !isRotate)
                {
                    if (touchZero.deltaPosition.y * touchOne.deltaPosition.y > 10)
                    {
                        isTilt = true;
                    }
                    else if (Mathf.Abs(deltaAngle) > 2)
                    {
                        isRotate = true;
                    }
                    else if (Mathf.Abs(deltaMagnitudeDiff) > 10)
                    {
                        isZoom = true;
                    }
                }
                if (isTilt)
                {
                    float angleXAdd;
                    // get value of less moved touch
                    if (touchZero.deltaPosition.y > 0 && touchOne.deltaPosition.y > 0)
                    {
                        angleXAdd = -Mathf.Min(touchZero.deltaPosition.y, touchOne.deltaPosition.y) * tiltSpeed;
                    }
                    else if (touchZero.deltaPosition.y < 0 && touchOne.deltaPosition.y < 0)
                    {
                        angleXAdd = -Mathf.Max(touchZero.deltaPosition.y, touchOne.deltaPosition.y) * tiltSpeed;
                    }
                    else
                    {
                        angleXAdd = 0;
                    }
                    float newAngleX = angleXAdd + transform.eulerAngles.x;
                    if (newAngleX < 45)
                    {
                        angleXAdd = 45 - transform.eulerAngles.x;
                    }
                    // 89.5 because of terrain - bad bilboarding
                    else if (newAngleX > 89.5)
                    {
                        angleXAdd = 89.5f - transform.eulerAngles.x;
                    }
                    float angleX = transform.eulerAngles.x;
                    transform.Rotate(angleXAdd, 0, 0);
                    float angleAfter = transform.eulerAngles.x;
                    if (angleAfter > 89.48)
                    {
                        transform.rotation = Quaternion.Euler(89.5f, transform.eulerAngles.y, transform.eulerAngles.z);
                    }

                }
                else if (isRotate)
                {
                    // check which touch is moving, if both, then center position
                    if (touchZero.phase == TouchPhase.Moved && touchOne.phase == TouchPhase.Moved)
                    {
                        if (rotateAroundPointState != 0)
                        {
                            pointPosition = Vector3.Lerp(GetWorldPoint(touchZero.position), GetWorldPoint(touchOne.position), 0.5f);
                            rotateAroundPointState = 0;
                        }
                    }
                    else if (touchZero.phase == TouchPhase.Moved)
                    {
                        if (rotateAroundPointState != 1)
                        {
                            pointPosition = GetWorldPoint(touchOne.position);
                            rotateAroundPointState = 1;
                        }
                    }
                    else //touchOne moved
                    {
                        if (rotateAroundPointState != 2)
                        {
                            pointPosition = GetWorldPoint(touchZero.position);
                            rotateAroundPointState = 2;
                        }
                    }
                    transform.RotateAround(pointPosition, Vector3.up, deltaAngle);
                }
                else if (isZoom)
                {
                    // Change the field of view based on the change in distance between the touches.
                    camera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
                    // Clamp the field of view to make sure it's between 40 and 100.
                    camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, 40, 100);
                }
            } // end of touches moved 
        } // end of two touches
    }

    private static float CalculateAngleBetweenTouches(Touch touchZero, Touch touchOne)
    {
        Vector2 v2 = touchZero.position - touchOne.position;
        return Mathf.Atan2(v2.y, v2.x) * Mathf.Rad2Deg;
    }

    // convert screen point to world point
    private Vector3 GetWorldPoint(Vector2 screenPoint)
    {
        // check only floor background layer, that is 10
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(screenPoint), out RaycastHit hitInfo, 1000, 1 << 10);
        return hitInfo.point;
    }
}