﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ARSceneController : MonoBehaviour
{
    private AndroidJavaObject captureService;
    private AndroidJavaObject databaseService;
    
    public TextMeshProUGUI tmpLabelHealthPoints;
    public TextMeshProUGUI tmpHealthPoints;
    public TextMeshProUGUI tmpWarningMissedImage;
    public TextMeshProUGUI tmpWaitingTime;
    public GameObject finishedPanel;
    public TextMeshProUGUI tmpMessageDmgRepair;
    public TextMeshProUGUI tmpMessageCapture;
    public TextMeshProUGUI tmpMessageInfoAimToTarget;
    public GameObject targets;

    private float timer;
    private bool wasDetected;
    private bool wasAtLeastOnceDetected;
    private float timerLostDetection;
    private bool isCreatingFingerprint;
    private bool isCreatingScreenShot;
    private bool isFinishedScanning;

    void Start()
    {
        captureService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getCaptureInstance");
        databaseService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getDatabaseInstance");
    }
    
    void Update()
    {
        if (!isFinishedScanning)
        {
            if (SharedObjects.IS_DETECTED)
            {
                tmpMessageInfoAimToTarget.gameObject.SetActive(false);
                wasDetected = true;
                wasAtLeastOnceDetected = true;
                timerLostDetection = 0;
                timer += Time.deltaTime;
                tmpWarningMissedImage.gameObject.SetActive(false);
                tmpHealthPoints.text = SharedObjects.AR_TOWER.HealthPoints + "/" + DataCounter.CalculateTowerMaxHealthPoints(SharedObjects.AR_TOWER.Level);
                tmpLabelHealthPoints.gameObject.SetActive(true);
                tmpHealthPoints.gameObject.SetActive(true);
                int dmgRepair = CalculateDmgRepair();
                tmpWaitingTime.gameObject.SetActive(true);
                if (timer > 10)
                {
                    timer = 10;
                }
                tmpWaitingTime.text = "" + (10 - timer).ToString("F2");  // round to two decimal
                if (!isCreatingFingerprint)
                {
                    isCreatingFingerprint = true;
                    print("vytvarim fingerprint");
                    captureService.Call("createFingerprint");
                }

                if (timer == 10 && !isCreatingScreenShot)
                {
                    StartCoroutine(SaveFingerprint()); // must be coroutine - method is creating screenshot
                    captureService.Call("attackRepairAction", new object[] { SharedObjects.AR_ACTION, dmgRepair, SharedObjects.AR_TOWER.KeyName });
                    if (SharedObjects.AR_ACTION.Equals(C.AR_ATTACK))
                    {
                        tmpHealthPoints.text = (SharedObjects.AR_TOWER.HealthPoints - dmgRepair) + "/" + DataCounter.CalculateTowerMaxHealthPoints(SharedObjects.AR_TOWER.Level);
                        tmpMessageDmgRepair.text = "Uděleno poškození: " + dmgRepair;
                        if (SharedObjects.AR_TOWER.HealthPoints - dmgRepair == 0)
                        {
                            tmpMessageCapture.text = "Gratuluji, věž byla zabrána!";
                            SharedObjects.AR_TOWER.Fraction = SharedObjects.AR_TOWER.Fraction == 1 ? 2 : 1;
                        } else
                        {

                            tmpMessageCapture.text = "Zbývá způsobit " + (SharedObjects.AR_TOWER.HealthPoints - dmgRepair) + " poškození.";
                        }
                    }
                    else
                    {
                        tmpHealthPoints.text = (SharedObjects.AR_TOWER.HealthPoints + dmgRepair) + "/" + DataCounter.CalculateTowerMaxHealthPoints(SharedObjects.AR_TOWER.Level);
                        tmpMessageDmgRepair.text = "Dodáno výdrže: " + dmgRepair;
                        if (DataCounter.CalculateTowerMaxHealthPoints(SharedObjects.AR_TOWER.Level) == dmgRepair + SharedObjects.AR_TOWER.HealthPoints)
                        {
                            tmpMessageCapture.text = "Věž je zcela opravena!";
                        } else
                        {
                            tmpMessageCapture.text = "Zbývá opravit " + (DataCounter.CalculateTowerMaxHealthPoints(SharedObjects.AR_TOWER.Level) - (dmgRepair + SharedObjects.AR_TOWER.HealthPoints)) + " výdrže.";
                        }
                    }
                }
            }
            else if (wasDetected)
            {
                if (timerLostDetection > 3)
                {
                    wasDetected = false;
                    timer = 0;
                    timerLostDetection = 0;
                    tmpWarningMissedImage.gameObject.SetActive(true);
                    
                    isCreatingFingerprint = false;
                    isCreatingScreenShot = false;
                }
                else
                {
                    timerLostDetection += Time.deltaTime;
                    tmpLabelHealthPoints.gameObject.SetActive(false);
                    tmpHealthPoints.gameObject.SetActive(false);
                    tmpWaitingTime.gameObject.SetActive(false);
                }
            } else if (!wasAtLeastOnceDetected)
            {
                tmpMessageInfoAimToTarget.gameObject.SetActive(true);
                tmpMessageInfoAimToTarget.text = "Namiř na nápis: " + SharedObjects.AR_TOWER.ArTarget;
            }
        } else
        { // scan and action are finished, show loading and wait for update from database (player, tower)
            if (!databaseService.Call<bool>("isPlayerTowerDataSaved"))
            {
                LoadingPanelController.Instance.StartLoading("arScene");
            } else
            {
                LoadingPanelController.Instance.StopLoading("arScene");
                finishedPanel.SetActive(true);
            }
            // hide tower, calculated number and hp
            targets.SetActive(false);
            tmpWaitingTime.gameObject.SetActive(false);
            tmpHealthPoints.gameObject.SetActive(false);
            tmpLabelHealthPoints.gameObject.SetActive(false);
        }
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene(C.MAIN_SCENE);
    }

    private IEnumerator SaveFingerprint()
    {
        yield return new WaitForEndOfFrame();
        isCreatingScreenShot = true;
        print("vytvarim screenshot a ukladam");
        Texture2D screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
        screenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, true);
        screenShot.Apply();
        byte[] bytes = screenShot.EncodeToPNG();

        databaseService.Call("saveFingerprintWithScreenshot", new object[] { bytes, SharedObjects.AR_TOWER.Floor, SharedObjects.AR_TOWER.X, SharedObjects.AR_TOWER.Y });

        isFinishedScanning = true;
    }

    private int CalculateDmgRepair()
    {
        int number;

        AndroidJavaObject player = databaseService.Call<AndroidJavaObject>("getPlayer");
        int damagePoints = player.Call<int>("getDamagePoints");
        int repairPoints = player.Call<int>("getRepairPoints");
        if (SharedObjects.AR_ACTION.Equals(C.AR_ATTACK))
        {
            number = Random.Range(DataCounter.CalculateDamageMin(damagePoints), DataCounter.CalculateDamageMax(damagePoints));
            if (SharedObjects.AR_TOWER.HealthPoints < number)
            {
                number = SharedObjects.AR_TOWER.HealthPoints;
            }
        } else
        {
            number = Random.Range(DataCounter.CalculateRepairMin(repairPoints), DataCounter.CalculateRepairMax(repairPoints));
            if (DataCounter.CalculateTowerMaxHealthPoints(SharedObjects.AR_TOWER.Level) < number + SharedObjects.AR_TOWER.HealthPoints)
            {
                number = DataCounter.CalculateTowerMaxHealthPoints(SharedObjects.AR_TOWER.Level) - SharedObjects.AR_TOWER.HealthPoints;
            }
        }

        return number;
    }

    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            LoadMainScene();
        }
    }
}
