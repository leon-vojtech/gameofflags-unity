﻿public abstract class C
{
    public static string PLAYER_FLOOR = "floor";
    public static string DEFAULT_FLOOR = "J1NP";
    public static string SELECTED_FLOOR = "selected_floor";
    public static string FOLLOW_CAMERA = "follow_camera";

    public static string FRACTION_RED = "Červení";
    public static string FRACTION_BLUE = "Modří";

    public static long MINUTE = 1000 * 60;
    public static long MINUTES_10 = MINUTE * 10;
    public static long WEEK_MILLISECONDS = MINUTE * 60 * 24 * 7;

    public static string AR_ATTACK = "attack";
    public static string AR_REPAIR = "repair";

    public static string MAIN_SCENE = "MainScene";
    public static string AR_SCENE = "ARScene";

    public static int MAX_LOADING_TIME = 30; // in seconds

    public static int MAX_CAMERA_POSITION = 100;
}