﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/**
 * 1 click - follow player
 * 2 clicks - move to default position and rotation
 * smoothfollow - https://docs.unity3d.com/ScriptReference/Vector3.SmoothDamp.html
 */
public class FollowCameraController : MonoBehaviour
{
    public GameObject player;
    private new Camera camera;
    
    private readonly float smoothTime = 0.2f;
    private Vector3 velocity = Vector3.zero;
    private float angleVelocityX, angleVelocityY, fovVelocity;
    private float xAngle, yAngle;
    private Vector3 offsetFollowCameraPosition = new Vector3(0, 20, 0);

    private float oppositeSide;
    private float adjacentSide;
    private int countFollowButtonClick;

    private float defaultFov;

    private void Start()
    {
        oppositeSide = transform.position.y;
        camera = Camera.main; // we need instance of camera for modifiing fov
        defaultFov = camera.fieldOfView;
    }

    // Update is called once per frame
    void Update()
    {
        adjacentSide = oppositeSide / Mathf.Tan(Mathf.Deg2Rad * transform.eulerAngles.x);
        offsetFollowCameraPosition.z = -Mathf.Cos(Mathf.Deg2Rad * transform.eulerAngles.y) * adjacentSide;
        offsetFollowCameraPosition.x = -Mathf.Sin(Mathf.Deg2Rad * transform.eulerAngles.y) * adjacentSide;

        //check touch
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
        {
            PlayerPrefs.SetInt(C.FOLLOW_CAMERA, 0);
            countFollowButtonClick = 0;
        }
    }

    private void LateUpdate()
    {
        if (PlayerPrefs.GetInt(C.FOLLOW_CAMERA, 0) == 1)
        {
            // Define a target position above and behind the target transform
            Vector3 targetPosition = player.transform.position + offsetFollowCameraPosition;
        
            // Smoothly move the camera towards that target position
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);

            if (countFollowButtonClick > 1)
            {
                xAngle = Mathf.SmoothDampAngle(transform.eulerAngles.x, 89.52f, ref angleVelocityX, smoothTime);
                yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, 0, ref angleVelocityY, smoothTime);
                transform.eulerAngles = new Vector3(xAngle, yAngle, 0);
                camera.fieldOfView = Mathf.SmoothDampAngle(camera.fieldOfView, defaultFov, ref fovVelocity, smoothTime);
            }
        }
        /*  // eliminate rounding problems
          if (transform.eulerAngles.x > 89.98)
          {
              transform.rotation = Quaternion.Euler(90, transform.eulerAngles.y, transform.eulerAngles.z);
          }
          */
        // eliminate terrain detail problem (not shown when x = 90)
        if (transform.eulerAngles.x > 89.5)
        {
            transform.rotation = Quaternion.Euler(89.52f, transform.eulerAngles.y, transform.eulerAngles.z);
        }
    }

    // method called on button click
    public void SetFollowCamera()
    {
        PlayerPrefs.SetInt(C.FOLLOW_CAMERA, 1);
        countFollowButtonClick++;
    }
}
