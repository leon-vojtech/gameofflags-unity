﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlinePlayersController : MonoBehaviour
{
    private AndroidJavaObject databaseService;

    public GameObject onlinePlayerPrefab;
    public Material[] materials;

    private Dictionary<string, OnlinePlayer> onlinePlayersDictionary = new Dictionary<string, OnlinePlayer>();

    private static OnlinePlayersController _instance;
    public static OnlinePlayersController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<OnlinePlayersController>();
            }

            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        databaseService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getDatabaseInstance");

        InvokeRepeating("SetOnlinePlayersData", 0.1f, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetOnlinePlayersData()
    {
        Dictionary<string, OnlinePlayer> newOnlinePlayersDictionary = GetOnlinePlayersDictionary();
        // remove deleted onlinePlayers 
        RemovePairDictionary(onlinePlayersDictionary, newOnlinePlayersDictionary);
        // get onlinePlayers info from db
        AddOrUpdatePairDictionary(onlinePlayersDictionary, newOnlinePlayersDictionary);

        // create onlinePlayers

        foreach (KeyValuePair<string, OnlinePlayer> entry in onlinePlayersDictionary)
        {
            OnlinePlayer onlinePlayer = entry.Value;
            if (onlinePlayer.Instance == null)
            {
                // Instantiate at obtained position and zero rotation.
                onlinePlayer.Instance = Instantiate(onlinePlayerPrefab, new Vector3(onlinePlayer.X / 100, 1.3f, -onlinePlayer.Y / 100), Quaternion.identity);
                onlinePlayer.Instance.name = entry.Key;
            }
            else
            {
                // onlinePlayer.Instance.transform.position = new Vector3(onlinePlayer.X / 10, 13, -onlinePlayer.Y / 10);
                StartCoroutine(SmoothMove(onlinePlayer.Instance, new Vector3(onlinePlayer.X / 100, 1.3f, -onlinePlayer.Y / 100)));
            }

            ChangeMaterial(onlinePlayer);
        }
    }

    private Dictionary<string, OnlinePlayer> GetOnlinePlayersDictionary()
    {
        Dictionary<string, OnlinePlayer> newOnlinePlayersDictionary = new Dictionary<string, OnlinePlayer>();
        AndroidJavaObject onlinePlayers = databaseService.Call<AndroidJavaObject>("getOnlinePlayersByFloor", PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR));
        if (onlinePlayers.Call<int>("size") > 0)
        {
            AndroidJavaObject iterator = onlinePlayers.Call<AndroidJavaObject>("entrySet").Call<AndroidJavaObject>("iterator");
            while (iterator.Call<bool>("hasNext"))
            {
                AndroidJavaObject pair = iterator.Call<AndroidJavaObject>("next");
                newOnlinePlayersDictionary.Add(pair.Call<string>("getKey"), new OnlinePlayer(pair.Call<AndroidJavaObject>("getValue")));
                iterator.Call("remove");
            }
        }

        return newOnlinePlayersDictionary;
    }

    private void AddOrUpdatePairDictionary(Dictionary<string, OnlinePlayer> dic, Dictionary<string, OnlinePlayer> newDic)
    {
        foreach (KeyValuePair<string, OnlinePlayer> entry in newDic)
        {
            OnlinePlayer onlinePlayer;
            if (dic.TryGetValue(entry.Key, out onlinePlayer))
            {
                // value exists
                entry.Value.Instance = onlinePlayer.Instance;
                dic[entry.Key] = entry.Value;
            }
            else
            {
                // add the value
                dic.Add(entry.Key, entry.Value);
            }
        }
    }

    private void RemovePairDictionary(Dictionary<string, OnlinePlayer> dic, Dictionary<string, OnlinePlayer> newDic)
    {
        Dictionary<string, OnlinePlayer> dicCopy = new Dictionary<string, OnlinePlayer>(dic);
        foreach (KeyValuePair<string, OnlinePlayer> entry in dicCopy)
        {
            if (!newDic.ContainsKey(entry.Key))
            {
                // remove onlinePlayer object
                Destroy(entry.Value.Instance);
                dic.Remove(entry.Key);
            }
        }
    }

    private void ChangeMaterial(OnlinePlayer onlinePlayer)
    {
        Material[] mats = onlinePlayer.Instance.GetComponentInChildren<SkinnedMeshRenderer>().materials;
        mats[1] = materials[3]; // parts
        mats[2] = materials[4]; // vinyl

        // body
        if (onlinePlayer.Fraction == 1)
        {
            mats[0] = materials[1];
        }
        else if (onlinePlayer.Fraction == 2)
        {
            mats[0] = materials[2];
        }
        else
        {
            // should not happen
            mats[0] = materials[0];
        }
        onlinePlayer.Instance.GetComponentInChildren<SkinnedMeshRenderer>().materials = mats;
    }

    IEnumerator SmoothMove(GameObject playerObject, Vector3 newPosition)
    {
        Animator animator = playerObject.GetComponent<Animator>();
        // rotate player in the direction of move
        playerObject.transform.LookAt(newPosition);
        while (Vector3.Distance(playerObject.transform.position, newPosition) > 0.01f )
        {
            // Move object position a step closer to the target.
            playerObject.transform.position = Vector3.Lerp(playerObject.transform.position, newPosition, Time.deltaTime);
            if (Vector3.Distance(playerObject.transform.position, newPosition) > 1)
            {
                if (null != animator && !IsPlaying(animator, 0, "Walk"))
                {
                    animator.Play("Walk");
                }
            }
            
            yield return null;
        }
    }

    // https://stackoverflow.com/a/50447357/11251845
    private bool IsPlaying(Animator anim, int animLayer, string stateName)
    {
        if (anim.GetCurrentAnimatorStateInfo(animLayer).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(animLayer).normalizedTime < 1.0f)
            return true;
        else
            return false;
    }
}
