﻿using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    private AndroidJavaObject player;

    private AndroidJavaObject authService;
    private AndroidJavaObject databaseService;
    private AndroidJavaObject errorService;

    public GameObject mainMenuPanel;
    public GameObject loginPanel;
    public GameObject newPlayerPanel;
    public TMP_InputField inputFieldNewNickname;
    public Button signInRedFraction;
    public Button signInBlueFraction;


    private string userUid;

    private int fraction = 0;
    private string newNickname;

    private void Start()
    {
        authService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getAuthenticationInstance");
        databaseService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getDatabaseInstance");
        errorService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getErrorInstance");
        inputFieldNewNickname.onValidateInput += delegate (string input, int charIndex, char addedChar) {
            return MyValidate(addedChar);
        };
    }

    private char MyValidate(char addedChar)
    {
        if (!Regex.IsMatch(addedChar.ToString(), @"^[a-zA-Z0-9]+$"))
        {
            addedChar = '\0';
        }

        return addedChar;
    }

    public void SignOut()
    {
        authService.Call("signOut");
    }

    public void SignIn()
    {
        authService.Call("createSignInIntent");
    }
    
    private void Update()
    {
        SetSettingsPanel();
    }

    public void SetNewPlayerData()
    {
        if (inputFieldNewNickname.text.Length > 3 && fraction > 0)
        {
            databaseService.Call("setPlayer", new object[] { inputFieldNewNickname.text, fraction });
            LoadingPanelController.Instance.StartLoading("newPlayerData");
        } else if (inputFieldNewNickname.text.Length > 3 && fraction == 0)
        {
            errorService.Call("showToast", "Musíš vybrat frakci (barvu)");
        } else if (inputFieldNewNickname.text.Length > 0)
        {
            errorService.Call("showToast", "Přezdívka musí mít alespoň 4 znaky");
        } else
        {
            errorService.Call("showToast", "Musíš zadat přezdívku");
        }
    }

    public void ChooseRedFraction()
    {
        fraction = 1;
        signInRedFraction.transform.localScale = new Vector3(2, 2, 1);
        signInBlueFraction.transform.localScale = new Vector3(1, 1, 1);

    }

    public void ChooseBlueFraction()
    {
        fraction = 2;
        signInRedFraction.transform.localScale = new Vector3(1, 1, 1);
        signInBlueFraction.transform.localScale = new Vector3(2, 2, 1);

    }

    public void SetSettingsPanel()
    {
        if (databaseService.Call<string>("getUserUid") != null)
        {
            player = databaseService.Call<AndroidJavaObject>("getPlayer");

            if (player != null)
            {
                if (!player.Call<bool>("isNameChanged"))
                {
                    // or it is still loading
                    newPlayerPanel.SetActive(true);
                    loginPanel.SetActive(false);
                    mainMenuPanel.SetActive(false);
                }
                else
                {
                    LoadingPanelController.Instance.StopLoading("newPlayerData");
                    mainMenuPanel.SetActive(true);
                    loginPanel.SetActive(false);
                    newPlayerPanel.SetActive(false);
                }
            }
            else
            {
                // should not happen 
            }
        }
        else
        {
            LoadingPanelController.Instance.StopLoading("newPlayerData");
            loginPanel.SetActive(true);
            mainMenuPanel.SetActive(false);
            newPlayerPanel.SetActive(false);
        }
    }
}
