﻿using System;

public class DataCounter
{
    /**
     * It increases by 40 per level, so 20, 80, 180, 320, 500, ... first level needs 20 exp, second 60 etc.
     */ 
    const double levelConst = 0.22360679775;

    public static int CalculateLevel(int experience)
    {
        return (int)Math.Floor(levelConst * Math.Sqrt(experience)) + 1;
    }

    public static int CalculateExperienceForNextLevel(int experience)
    {
        return (int) Math.Round(Math.Pow(CalculateLevel(experience) / levelConst, 2));
    }

    /**
     * Basic dmg 4, every point + 2, range 80-120 %
     */ 
    public static int CalculateDamage(int damagePoints)
    {
        int basicDamage = 4;
        return basicDamage + damagePoints * 2;
    }

    public static int CalculateDamageMin(int damagePoints)
    {
        return (int)Math.Round(CalculateDamage(damagePoints) * 0.8);
    }

    public static int CalculateDamageMax(int damagePoints)
    {
        return (int)Math.Round(CalculateDamage(damagePoints) * 1.2);
    }


    /**
     * Basic repair 2, every point + 2, range 70-130 % 
     */
    public static int CalculateRepair(int repairPoints)
    {
        int basicRepair = 3;
        return basicRepair + repairPoints * 2;
    }

    public static int CalculateRepairMin(int repairPoints)
    {
        return (int)Math.Round(CalculateRepair(repairPoints) * 0.7);
    }

    public static int CalculateRepairMax(int repairPoints)
    {
        return (int)Math.Round(CalculateRepair(repairPoints) * 1.3);
    }

    public static int CalculateTowerMaxHealthPoints(int level)
    {
        return 20 * level;
    }
}
