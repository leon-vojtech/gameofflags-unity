﻿using UnityEngine;

public class ResumeController : MonoBehaviour
{
    private AndroidJavaObject databaseService;

    private void Start()
    {
        databaseService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getDatabaseInstance");
    }
    
    void Update()
    {
        if (databaseService.Call<bool>("isApplicationResume"))
        {
            if (!databaseService.Call<bool>("databaseActual"))
            {
                LoadingPanelController.Instance.StartLoading("resume");
            }
            else
            {
                databaseService.Call("setApplicationResume", false);
                LoadingPanelController.Instance.StopLoading("resume");
            }
        }
    }
}
