﻿using UnityEngine;

public class OnlinePlayer
{
    public string Username { get; }
    public int Fraction { get; }
    public int Experience { get; }
    public long LastChangedAt { get; }
    public int X { get; }
    public int Y { get; }
    public string Floor { get; }

    public GameObject Instance { get; set; }

    public OnlinePlayer(AndroidJavaObject onlinePlayer)
    {
        Username = onlinePlayer.Call<string>("getUsername");
        Fraction = onlinePlayer.Call<int>("getFraction");
        Experience = onlinePlayer.Call<int>("getExperience");
        LastChangedAt = onlinePlayer.Call<long>("getLastChangedAt");
        X = onlinePlayer.Call<AndroidJavaObject>("getPosition").Call<int>("getX");
        Y = onlinePlayer.Call<AndroidJavaObject>("getPosition").Call<int>("getY");
        Floor = onlinePlayer.Call<AndroidJavaObject>("getPosition").Call<string>("getFloor");
    }
}
