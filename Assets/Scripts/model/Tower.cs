﻿using UnityEngine;

public class Tower
{
    public int X { get; }
    public int Y { get; }
    public string Floor { get; }
    public string Name { get; }
    public int HealthPoints { get; }
    public int Level { get; }
    public string CapturedBy { get; }
    public long CapturedAt { get; }
    public int Fraction { get; set; }
    public string KeyName { get; set; }
    public string ArTarget { get; set; }

    public GameObject Instance { get; set; }

    public Tower(AndroidJavaObject tower, string keyName)
    {
        X = tower.Call<int>("getX");
        Y = tower.Call<int>("getY");
        Floor = tower.Call<string>("getFloor");
        Name = tower.Call<string>("getName");
        HealthPoints = tower.Call<int>("getHealthPoints");
        Level = DataCounter.CalculateLevel(tower.Call<int>("getPopularity"));
        CapturedBy = tower.Call<string>("getCapturedBy");
        CapturedAt = tower.Call<long>("getCapturedAt");
        Fraction = tower.Call<int>("getFraction");
        ArTarget = tower.Call<string>("getArTarget");
        KeyName = keyName;
    }

    public override string ToString()
    {
        return "{" +
                "x=" + X +
                ", y=" + Y +
                ", floor='" + Floor + '\'' +
                ", name='" + Name + '\'' +
                ", hitPoints=" + HealthPoints +
                ", level=" + Level +
                ", capturedBy='" + CapturedBy + '\'' +
                ", capturedAt=" + CapturedAt +
                ", fraction=" + Fraction +
                ", arTarget=" + ArTarget +
                '}';
    }
}
