﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private AndroidJavaObject localizationService;
    private AndroidJavaObject databaseService;

    public GameObject playerObject;

    public float speed = 1.0f;
    private Vector3 newPosition;

    private AndroidJavaObject player;

    public TextMeshProUGUI tmpNickname;
    public TextMeshProUGUI tmpLevel;
    public TextMeshProUGUI tmpExperience;
    public TextMeshProUGUI tmpDamage;
    public TextMeshProUGUI tmpRepair;
    public TextMeshProUGUI tmpFreePoints;
    public Button btnAddDamage;
    public Button btnAddRepair;
    public TextMeshProUGUI tmpLevelTopPanel;
    public Button btnPlayer;
    public Material[] materials;

    private bool addDamage = false;
    private bool addRepair = false;
    private int damagePoints;
    private int repairPoints;

    private Animator animator;

    private void Start()
    {
        localizationService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getLocalizationInstance");
        databaseService = new AndroidJavaClass("cz.uhk.vojtele1.gameofflags.plugin.service.InstanceService").CallStatic<AndroidJavaObject>("getDatabaseInstance");

        newPosition = playerObject.transform.position;
        // Get position from plugin every 1 second
        InvokeRepeating("SetPosition", 0.1f, 1f);

        animator = playerObject.GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (PlayerPrefs.GetString(C.PLAYER_FLOOR, C.DEFAULT_FLOOR).Equals(PlayerPrefs.GetString(C.SELECTED_FLOOR, C.DEFAULT_FLOOR)))
        {
            playerObject.SetActive(true);
            // Check if the positions are not approximately equal.
            if (Vector3.Distance(playerObject.transform.position, newPosition) > 0.01f)
            {
                // rotate player in the direction of move
                playerObject.transform.LookAt(newPosition);
                // Move object position a step closer to the target.
                playerObject.transform.position = Vector3.Lerp(playerObject.transform.position, newPosition, speed * Time.deltaTime);

                if (Vector3.Distance(playerObject.transform.position, newPosition) > 1 && null != animator && !IsPlaying(animator, 0, "Walk"))
                {
                    animator.Play("Walk");
                }
            }
        } else
        {
            playerObject.SetActive(false);
        }

        player = databaseService.Call<AndroidJavaObject>("getPlayer");
        if (player != null)
        {
            if (addDamage || addRepair)
            {
                if (damagePoints != player.Call<int>("getDamagePoints") || repairPoints != player.Call<int>("getRepairPoints"))
                {
                    StopLoading();
                }
                else
                {
                    StartLoading();
                }
            }
        }

        FillPlayerPanel();
    }

    // https://stackoverflow.com/a/50447357/11251845
    private bool IsPlaying(Animator anim, int animLayer, string stateName)
    {
        if (anim.GetCurrentAnimatorStateInfo(animLayer).IsName(stateName) &&
                anim.GetCurrentAnimatorStateInfo(animLayer).normalizedTime < 1.0f)
            return true;
        else
            return false;
    }

    private Vector2 GetCalculatedPosition()
    {
        Vector2 position = new Vector2();
        
        AndroidJavaObject positionAJO = localizationService.Call<AndroidJavaObject>("getMyPosition");
        position.Set((float) positionAJO.Call<double>("getX"),(float) positionAJO.Call<double>("getY"));
        
        return position / 100;
    }

    private void SetPosition()
    {
        Vector2 calculatedPosition = GetCalculatedPosition();
        newPosition.Set(calculatedPosition.x, playerObject.transform.position.y, -calculatedPosition.y);
    }
    
    private void FillPlayerPanel()
    {
        player = databaseService.Call<AndroidJavaObject>("getPlayer");
        if (player != null)
        {
            tmpNickname.text = player.Call<string>("getNickname");
            int experience = player.Call<int>("getExperience");
            int level = DataCounter.CalculateLevel(experience);
            tmpLevel.text = level.ToString();
            tmpLevelTopPanel.text = level.ToString();
            tmpExperience.text = experience + "/" + DataCounter.CalculateExperienceForNextLevel(experience);
            int damagePoints = player.Call<int>("getDamagePoints");
            int repairPoints = player.Call<int>("getRepairPoints");
            tmpDamage.text = DataCounter.CalculateDamageMin(damagePoints) + " - " + DataCounter.CalculateDamageMax(damagePoints);
            tmpRepair.text = DataCounter.CalculateRepairMin(repairPoints) + " - " + DataCounter.CalculateRepairMax(repairPoints);
            int freePoints = level - damagePoints - repairPoints - 1 + player.Call<int>("getBonusFreePoints");
            if (freePoints < 0) // To be sure that negative value is not shown (should not happen)
            {
                freePoints = 0;
            }
            tmpFreePoints.text = freePoints.ToString();
            if (player.Call<int>("getFraction") == 2)
            {
                btnPlayer.GetComponent<Image>().material = materials[2];
            }
            else if (player.Call<int>("getFraction") == 1)
            {
                btnPlayer.GetComponent<Image>().material = materials[1];
            }
            else
            {
                btnPlayer.GetComponent<Image>().material = materials[0];
            }
            if (freePoints > 0)
            {
                btnAddDamage.interactable = true;
                btnAddRepair.interactable = true;
            }
            else
            {
                btnAddDamage.interactable = false;
                btnAddRepair.interactable = false;
            }
        } else
        {
            tmpNickname.text = "Nickname";
            tmpLevel.text = "26";
            tmpLevelTopPanel.text = "26";
            tmpExperience.text = "11240/14560";
            tmpFreePoints.text = "12";
            tmpDamage.text = "154 - 287";
            tmpRepair.text = "112 - 164";
            btnAddDamage.interactable = false;
            btnAddRepair.interactable = false;
            btnPlayer.GetComponent<Image>().material = materials[0];
        }
       
    }

    public void AddDamage()
    {
        addDamage = true;
        SetLocalData();
        databaseService.Call("addDamage");
    }

    public void AddRepair()
    {
        addRepair = true;
        SetLocalData();
        databaseService.Call("addRepair");
    }

    private void StartLoading()
    {
        LoadingPanelController.Instance.StartLoading("addDamageRepairPoint");
    }

    private void SetLocalData()
    {
        damagePoints = player.Call<int>("getDamagePoints");
        repairPoints = player.Call<int>("getRepairPoints");
    }

    private void StopLoading()
    {
        LoadingPanelController.Instance.StopLoading("addDamageRepairPoint");
        addDamage = false;
        addRepair = false;
    }
}